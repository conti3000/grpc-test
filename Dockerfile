FROM tensorflow/tensorflow:latest-gpu
LABEL maintainer Jorge

RUN mkdir /docker_app
RUN apt update && apt install -y libsm6 libxext6 libxrender-dev ffmpeg
COPY requirements.txt /docker_app
WORKDIR /docker_app
RUN pip install --upgrade -r requirements.txt
RUN rm requirements.txt
COPY . /docker_app
EXPOSE 50052
